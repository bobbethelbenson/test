FROM drecom/centos-ruby

MAINTAINER Bob Benson "bobbethelbenson@gmail.com"
RUN  yum -y install git make  && yum clean all && gem install jekyll therubyracer json --no-doc --no-ri

#RUN gem install jekyll therubyracer json --no-doc --no-ri

VOLUME ["/usr/share/nginx/html"]


RUN git clone https://gitlab.com/bobbethelbenson/bb-blog.git /data/jekyll-example

CMD ["jekyll", "build", "--watch", "--incremental", "--source=/data/jekyll-example", "--destination=/usr/share/nginx/html"]
